package com.aixuexiao.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**

 * 判断时间是否在当天之内
 * @author kgm
 *
 */
public class DateUtils {
    public static final int DAY_CODE = 1; //当天零点
    public static final int WEEK_CODE = 2; //每周第一天
    public static final int MONTH_CODE = 3; //每月第一天
    public static final int SEASON_CODE = 4; //每季度第一天
    public static final int YEAR_CODE = 5; //每年第一天
    public static final int PRE_YEAR_CODE = 6; //最近一年的时间
    public static final String MONTH_FORMAT = "yyyy-MM";//年月格式化字符串
    public static final String DAY_FORMAT = "yyyy-MM-dd";//年月日格式化字符串
    public static final String TIME_FORMAT= "yyyy-MM-dd HH:mm:ss";//年月日时分秒格式化字符串
    

	
	/**   
	 * @Title: getStartTime   
	 * @Description: 获取当天的开始时间
	 * @param format
	 * @return
	 * @author  author
	 */
	 
	public static String getStartTime(String format){
		return getStartTime(new Date(),format);
	}
	public static String getStartTime(Date date,String format){
        Calendar todayStart = Calendar.getInstance();
        todayStart.setTime(date);
        todayStart.set(Calendar.DAY_OF_YEAR, todayStart.get(Calendar.DAY_OF_YEAR)-1);
//        todayStart.set(Calendar.HOUR_OF_DAY, 0);
//        todayStart.set(Calendar.MINUTE, 0);
//        todayStart.set(Calendar.SECOND, 0);
//        todayStart.set(Calendar.MILLISECOND, 0);
        return format(todayStart.getTime(),format);
    }
	
	/**   
	 * @Title: getEndTime   
	 * @Description: 获取当天的结束时间
	 * @return
	 * @author  author
	 */
	 
	public static String getEndTime(String format){
		Calendar todayEnd = Calendar.getInstance();
		todayEnd.set(Calendar.HOUR_OF_DAY, 23);
		todayEnd.set(Calendar.MINUTE, 59);
		todayEnd.set(Calendar.SECOND, 59);
		todayEnd.set(Calendar.MILLISECOND, 999);
		return format(todayEnd.getTime(),format);
	}
	
	/**
	 * 得到本周第一天（从周一开始计算）
	 */
	public static String getWeekFirstDay(String format) {
		Calendar calendar = Calendar.getInstance();
		calendar.setFirstDayOfWeek(2);
		calendar.set(Calendar.DAY_OF_WEEK, calendar.getActualMinimum(Calendar.DAY_OF_WEEK)+1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return format(calendar.getTime(),format);
	}
	
	/**  
	 * 得到本月的第一天  
	 * @return  
	 */  
	public static String getMonthFirstDay(String format) {   
	    Calendar calendar = Calendar.getInstance();   
	    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));   
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
	    return format(calendar.getTime(),format);
	}   
	 
	/**
	 * 得到本季第一天
	 */
	public static String getSeasonFirstDay(String format) {   
		Calendar calendar = Calendar.getInstance();     
        calendar.setTime(new Date());  
        int curMonth = calendar.get(Calendar.MONTH);  
        if (curMonth >= Calendar.JANUARY && curMonth <= Calendar.MARCH){    
            calendar.set(Calendar.MONTH, Calendar.JANUARY);  
        }  
        if (curMonth >= Calendar.APRIL && curMonth <= Calendar.JUNE){    
            calendar.set(Calendar.MONTH, Calendar.APRIL);  
        }  
        if (curMonth >= Calendar.JULY && curMonth <= Calendar.SEPTEMBER) {    
            calendar.set(Calendar.MONTH, Calendar.JULY);  
        }  
        if (curMonth >= Calendar.OCTOBER && curMonth <= Calendar.DECEMBER) {    
            calendar.set(Calendar.MONTH, Calendar.OCTOBER);  
        }  
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));  
        calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return format(calendar.getTime(),format);
	}   
	/**  
     * 得到本年的第一天  
     * @return  
     */  
    public static String getYearFirstDay(String format) {   
        Calendar calendar = Calendar.getInstance(); 
        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 1);   
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return format(calendar.getTime(),format);
    } 
    /**   
     * @Title: getMonthBoundDateStr   
     * @Description: 根据年月生成其实时间  
     * @param year
     * @param month
     * @return
     * @author  author
     */
     
    public static Date[] getMonthBoundDateStr(int year,int month){
        Date[] dates = new Date[2];
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month-1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        dates[0] = calendar.getTime();
        calendar.set(Calendar.DAY_OF_MONTH,
                calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        dates[1] = calendar.getTime();
        return dates;
    }
    
    /**
     * 时间差多少小时
     * @param last
     * @param current
     * @return
     */
    public static double betweenHH(Date last,Date current,boolean all){
        long between  = current.getTime()-last.getTime();
        long day = between / (24 * 60 * 60 * 1000);
        if(all){
            return (between / (60 * 60 * 1000) + day * 24);
        }else{
            return (between / (60 * 60 * 1000) - day * 24);
        }
    }
    /**   
     * @Title: format   
     * @Description: 格式化时间   
     * @param date
     * @param formatStr
     * @return
     * @author  author
     */
     
    public static String format(Date date,String formatStr){
        SimpleDateFormat df = new SimpleDateFormat(formatStr);
        return df.format(date);
    }
    /**
     * 把字符串转换成Date
     * @param str
     * @return
     */
    public static Date strToDate(String str) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(TIME_FORMAT);
        GregorianCalendar calendar;
        try {
            return dateFormat.parse(str);
        } catch (ParseException ex) {
            calendar = new GregorianCalendar();
            calendar.set(1900, 1, 1);
        }
        return calendar.getTime();
    }
    
    /**   
     * @Title: strToDate   
     * @Description: 根据日期格式，并返回时间 
     * @param str
     * @param format
     * @return
     * @author  author
     */
     
    public static Date strToDate(String str,String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        GregorianCalendar calendar;
        try {
            return dateFormat.parse(str);
        } catch (ParseException ex) {
            calendar = new GregorianCalendar();
            calendar.set(1900, 1, 1);
        }
        return calendar.getTime();
    }
    
    /**
     * 
     * @Title: getYearDay   
     * @Description: 获取一年前的时间 
     * @return
     * @author  yml
     */
    public static String getPreviousYearDay(String format){
        
        Calendar curr = Calendar.getInstance();
        curr.set(Calendar.YEAR,curr.get(Calendar.YEAR)-1);
        return format(curr.getTime(),format);
    }
    /**   
     * @Title: formatMonthStr   
     * @Description: 把月份格式化成ERI中月份存储格式 
     * @param month
     * @return
     * @author  author
     */
     
    public static String formatMonthStr(int month){
        return month>=10?""+month:"0"+month;
    }
    public static void main(String[] args) {
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
        Date date = new Date();
        String enddate = sdf.format(date);
        System.out.println(enddate);
        /*Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH)+2);
       
        int day = cal.get(Calendar.DATE);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);
        int dow = cal.get(Calendar.DAY_OF_WEEK);
        int dom = cal.get(Calendar.DAY_OF_MONTH);
        int doy = cal.get(Calendar.DAY_OF_YEAR);
         
        System.out.println("Current Date: " + cal.getTime());
        System.out.println("Day: " + day);
        System.out.println("Month: " + month);
        System.out.println("Year: " + year);
        System.out.println("Day of Week: " + dow);
        System.out.println("Day of Month: " + dom);
        System.out.println("Day of Year: " + doy);*/
      }
}
