package com.aixuexiao.model;

import java.util.ArrayList;
import java.util.List;

public class Grid<T> implements java.io.Serializable {

	
	 /**
	 * 
	 */
	 
	private static final long serialVersionUID = 1L;
	private int total = 0;
	private List<T> rows = new ArrayList<T>();
	private int pages=0;//总页数
	private int pageNum=8;//每页记录数
	private int currentPage=1;//当前页
	private int begin;
	private int end;

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<T> getRows() {
		return rows;
	}

	public void setRows(List<T> rows) {
		this.rows = rows;
	}

	public int getPages() {
		if(total%pageNum==0){
			pages = total/pageNum;
		}else{
			pages = total/pageNum+1;
		}
		
		return pages;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getBegin() {
		int pages = getPages();
		if(pages>10){//当总页数大于10页时，根据当前页计算起始页
			begin = currentPage+9<=pages?currentPage:pages-9;
		}else{
			begin =1;
		}
		return begin;
	}

	public int getEnd() {
		int pages = getPages();
		int begin = getBegin();
		if(begin+9<=pages){//从起始页 + 9大于总页数
			end = begin+9;
		}else{
			end =pages;
		}
		return end;
	}


}
