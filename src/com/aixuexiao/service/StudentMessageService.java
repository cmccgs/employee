package com.aixuexiao.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.aixuexiao.dao.StudentMessageDao;
import com.aixuexiao.model.StudentMessage;

@Service("studentMessageService")
public class StudentMessageService {

	@Resource(name="studentMessageDao")
	private StudentMessageDao studentMessageDao;
	
	public List<StudentMessage> listStudentMessage(int start,int size,StudentMessage studentMessage){
		return studentMessageDao.findStudentMessages(start,size,studentMessage);
	}
	public int countStudentMessage(StudentMessage studentMessage){
		return studentMessageDao.countStudentMessages(studentMessage);
	}
	public int updateStudentMessage(StudentMessage studentMessage) {
		return studentMessageDao.updateStudentMessage(studentMessage);
	}
	/**
	 * 删除数据库中对应id的公司信息
	 * @param classesId 公司id
	 */
	public int deleteStudentMessage(int id){
		return studentMessageDao.deleteStudentMessageById(id);
	}
}
