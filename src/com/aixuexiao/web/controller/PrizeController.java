package com.aixuexiao.web.controller;


import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.aixuexiao.model.Grid;
import com.aixuexiao.model.Prize;
import com.aixuexiao.service.PrizeService;

/**
 * 奖品领取记录的所有操作
 */
@Controller()
public class PrizeController {
	public static final int pagesize = 8;
	
	@Resource(name="prizeService")
	private PrizeService prizeService;
	
	
	@RequestMapping(value="/manager/prizes")
	public ModelAndView listPrize(String pagenum,HttpServletRequest request){
		Prize prize = new Prize();
		String id = request.getParameter("id");
		if(!StringUtils.isEmpty(id))
			prize.setId(Integer.parseInt(id));
		ModelAndView mv=new ModelAndView();
		mv.setViewName("prizes");
		mv.addObject("sidebar","prizes");
		int num = 1;
		if(null!=pagenum && !StringUtils.isEmpty(pagenum)){
			num = Integer.parseInt(pagenum);
		}
		List<Prize> list = prizeService.listPrize((num-1)*pagesize, pagesize,prize);
		int count = prizeService.countPrize(prize);
		Grid<Prize> grid = new Grid<Prize>();
		grid.setRows(list);
		grid.setTotal(count);
		grid.setCurrentPage(num);
		grid.setPageNum(pagesize);
		mv.addObject("grid",grid);
		mv.addObject("length", list.size());
		mv.addObject("prize", prize);
		if(prize.getId()!=0)
		mv.addObject("prizeId",prize.getId());
		return mv;
	}
}
