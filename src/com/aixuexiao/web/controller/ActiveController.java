package com.aixuexiao.web.controller;


import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.aixuexiao.model.Active;
import com.aixuexiao.model.Club;
import com.aixuexiao.model.Grid;
import com.aixuexiao.service.ActiveService;
import com.aixuexiao.service.ClubService;
import com.aixuexiao.util.DateUtils;

@Controller
public class ActiveController {
	
	
	public static int pagesize = 10;
	@Resource(name="clubService")
	private ClubService clubService;
	@Resource(name="activeService")
	private ActiveService activeService;
	@RequestMapping(value="/manager/active")
	public ModelAndView listActive(String pagenum,HttpServletRequest request){
		ModelAndView mv=new ModelAndView();
		mv.setViewName("active");
		mv.addObject("sidebar","active");
		int num = 1;
		if(null!=pagenum && !StringUtils.isEmpty(pagenum)){
			num = Integer.parseInt(pagenum);
		}
		Active active = new Active();
		active.setName(request.getParameter("name"));
		List<Active> list = activeService.listActive((num-1)*pagesize, pagesize,active);
		int count = activeService.countActive(active);
		Grid<Active> grid = new Grid<Active>();
		grid.setRows(list);
		grid.setTotal(count);
		grid.setCurrentPage(num);
		grid.setPageNum(pagesize);
		mv.addObject("grid",grid);
		mv.addObject("active", active);
		return mv;
	}

	
	@RequestMapping(value="/manager/addactivepage",method=RequestMethod.GET)
	public ModelAndView addActivePage(){
		ModelAndView mv=new ModelAndView();
		mv.setViewName("addactive");
		List<Club> clubList = clubService.findAllClub();
		mv.addObject("clubList",clubList);
		mv.addObject("sidebar","active");
		return mv;
	}
	
	@RequestMapping(value="/manager/addactive",method=RequestMethod.POST)
	public ModelAndView addActive(Active active,String startTime,String endTime){
		ModelAndView mv=new ModelAndView();
		mv.setViewName("redirect:/manager/active");
		active.setBegintime(DateUtils.strToDate(startTime, DateUtils.TIME_FORMAT));
		active.setEndtime(DateUtils.strToDate(endTime, DateUtils.TIME_FORMAT));
		activeService.addActive(active);
		return mv;
	}
	@RequestMapping(value="/manager/updateactive",method=RequestMethod.POST)
	public ModelAndView updateactive(Active active,String startTime,String endTime){
		ModelAndView mv=new ModelAndView();
		active.setBegintime(DateUtils.strToDate(startTime, DateUtils.TIME_FORMAT));
		active.setEndtime(DateUtils.strToDate(endTime, DateUtils.TIME_FORMAT));
		activeService.updateActive(active); 
		mv.addObject("notice","编辑俱乐部活动信息成功");
		mv.setViewName("redirect:/manager/active");
		return mv;
	}
	@RequestMapping(value="/manager/deleteactive",method=RequestMethod.GET)
	public ModelAndView deleteStudentMessage(int id){
		ModelAndView mv=new ModelAndView();
		activeService.deleteActiveById(id);
		mv.addObject("notice","删除俱乐部活动信息成功");
		mv.setViewName("redirect:/manager/active");
		return mv;
	}
	
	
}
