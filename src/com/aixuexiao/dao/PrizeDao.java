package com.aixuexiao.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.aixuexiao.model.Prize;

@Component("prizeDao")
public class PrizeDao extends BaseDao {


	public Prize findPrizeById(int id) {
		return this.readSqlSession.selectOne("com.aixuexiao.dao.PrizeDao.selectPrizeById",id);
	}
	public List<Prize> findPrize(int start,int size,Prize prize) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("start", start);
		map.put("size", size);
		map.put("prize", prize);
		return this.readSqlSession.selectList("com.aixuexiao.dao.PrizeDao.selectPrize",map);
	}
	public int countPrize(Prize prize) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("prize", prize);
		return this.readSqlSession.selectOne("com.aixuexiao.dao.PrizeDao.selectPrizeCount",map);
	}
	
	public int addPrize(Prize prize) {
		return this.writerSqlSession.insert("com.aixuexiao.dao.PrizeDao.addPrize", prize);
	}
	
	public int deletePrizeById(int prizeid) {
		return this.writerSqlSession.delete("com.aixuexiao.dao.PrizeDao.deletePrizeById", prizeid);
	}
	
	public int updatePrize(Prize prize) {
		return this.writerSqlSession.update("com.aixuexiao.dao.PrizeDao.updatePrize", prize);
	}
	public int updatePrizeNum(int prizeid){
		return this.writerSqlSession.update("com.aixuexiao.dao.PrizeDao.updatePrizeNum",prizeid);
	}
	
}
