package com.aixuexiao.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.aixuexiao.model.StudentMessage;

@Component("studentMessageDao")
public class StudentMessageDao extends BaseDao {

	public List<StudentMessage> findStudentMessages(int start,int size,StudentMessage studentMessage) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("start", start);
		map.put("size", size);
		map.put("student", studentMessage);
		return this.readSqlSession.selectList("com.aixuexiao.dao.StudentMessageDao.selectStudentMessages",map);
	}
	public int countStudentMessages(StudentMessage studentMessage) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("student", studentMessage);
		return this.readSqlSession.selectOne("com.aixuexiao.dao.StudentMessageDao.selectStudentMessagesCount",map);
	}
	
	public List<StudentMessage> findStudentMessageByStudentId(int studentid,int limit) {
		Map<String,Integer> map = new HashMap<String,Integer>();
		map.put("studentid", studentid);
		map.put("limit", limit);
		return this.readSqlSession.selectList("com.aixuexiao.dao.StudentMessageDao.selectStudentMessageByStudentId",map);
	}
	
	public int addStudentMessage(StudentMessage studentMessage) {
		return this.writerSqlSession.insert("com.aixuexiao.dao.StudentMessageDao.addStudentMessage", studentMessage);
	}
	public int updateStudentMessage(StudentMessage studentMessage){
		return writerSqlSession.update("com.aixuexiao.dao.StudentMessageDao.updateStudentMessage", studentMessage);
	}
	public int deleteStudentMessageById(int id) {
		return this.writerSqlSession.delete("com.aixuexiao.dao.StudentMessageDao.deleteStudentMessageById", id);
	}
	
}
