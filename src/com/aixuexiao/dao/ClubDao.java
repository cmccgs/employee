package com.aixuexiao.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.aixuexiao.model.Club;

@Component("clubDao")
public class ClubDao extends BaseDao {

	
	public List<Club> findAllClub() {
		return this.readSqlSession.selectList("com.aixuexiao.dao.ClubDao.selectAllClub");
	}
	
	public List<Club> findClub(int start,int size,Club club) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("start", start);
		map.put("size", size);
		map.put("club", club);
		return this.readSqlSession.selectList("com.aixuexiao.dao.ClubDao.selectClub",map);
	}
	public int countClub(Club club) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("club", club);
		return this.readSqlSession.selectOne("com.aixuexiao.dao.ClubDao.selectClubCount",map);
	}
	public void addClub(Club club){
		writerSqlSession.insert("com.aixuexiao.dao.ClubDao.addClub", club);
	}
	public void updateClub(Club club){
		writerSqlSession.insert("com.aixuexiao.dao.ClubDao.updateClub", club);
	}
	public void clubchangestate(int clubId,int status){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("id", clubId);
		map.put("status", status);
		writerSqlSession.update("com.aixuexiao.dao.ClubDao.clubchangestate", map);
	}
	public Club findClubById(int id){
		return readSqlSession.selectOne("com.aixuexiao.dao.ClubDao.selectClubById", id);
	}
}
