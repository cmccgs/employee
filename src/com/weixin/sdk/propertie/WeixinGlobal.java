/*
 * 文件名：WeixinGlobal.java
 * 版权：Copyright by 791440142
 * 描述：
 * 修改人：yu_qhai
 * 修改时间：2016年3月20日
 */

package com.weixin.sdk.propertie;


 /**
 * @ClassName:     WeixinGlobal.java
 * @Description:   微信全局变量配置 
 * @author         (作者)
 * @version        V1.0  
 * @Date           2016年3月20日 上午8:25:32 
 */
 
public interface WeixinGlobal {
	String APPID = "wx26ea7162261502f4";
	String APPSECRET = "5a90d14488d35b79dd4685f1345a3d5a";
	String TOKEN = "thkjtoken";
	//获取全局Access Token请求模版
	String ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
	//获取openid的请求模版
	String ACCESS_OPENID = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=APPSECRET&code=CODE&grant_type=authorization_code";
	String ACCESS_USER = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID";
	
}
