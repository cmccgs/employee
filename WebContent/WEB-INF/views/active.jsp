<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>活动信息</title>
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<%@include file="/WEB-INF/views/common/css.jsp" %>
	</head>
	<body>
		<%@ include file="/WEB-INF/views/common/navbar.jsp" %>
		<div class="main-container container-fluid">
			<a class="menu-toggler" id="menu-toggler" href="#">
				<span class="menu-text"></span>
			</a>
			<%@ include file="/WEB-INF/views/common/sidebar.jsp" %>
			<div class="main-content">
				 
				<div class="page-content">
					<div class="page-header position-relative">
						<h1>
							<small>
								<i class="icon-list-alt"></i>
								活动信息
							</small>
						</h1>
					</div> 
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<form class="form-inline" id="searchActiveForm" method="post"  action="<%=request.getContextPath() %>/manager/active">
								<input type="hidden" id="pagenum" name="pagenum" value="${pagenum}">
								&nbsp;&nbsp;名称：<input type="text" name="name" value="${active.name}"  class="input-medium search-query">&nbsp;&nbsp;&nbsp;&nbsp;
								<button  type="submit" class="btn btn-purple btn-small">
									查找
									<i class="icon-search icon-on-right bigger-110"></i>
								</button>
								<button  type="button" class="btn btn-purple btn-small" onclick="location.href='<%=request.getContextPath() %>/manager/active'" >
									清空
									<i class="icon-remove icon-on-right bigger-110"></i>
								</button>
								<button  type="button" onclick="location.href='<%=request.getContextPath() %>/manager/addactivepage'" class="btn btn-purple btn-small">
									添加活动
									<i class="icon-plus-sign icon-on-right bigger-110"></i>
								</button>
							</form>
							
							<table id="sample-table-1" class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th width="10%">编号</th>
										<th width="20%">活动名称</th>
										<th width="10%">俱乐部</th>
										<th width="15%">开始时间</th>
										<th width="15%">结束时间</th>
										<th width="40%" >操作</th>
									</tr>
								</thead>
								<tbody>
								<c:forEach items="${grid.rows}"  var="active"  >
									<tr>
										<td>${active.id}</td>
										<td>${active.name}</td>
										<td>${active.clubname }</td>
										<td><fmt:formatDate value="${active.begintime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td><fmt:formatDate value="${active.endtime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td >
											<button class="btn btn-mini btn-primary" onclick="if(window.confirm('确认要删除活动${active.name}？')==true)location.href='<%=request.getContextPath() %>/manager/deleteactive?id=${active.id}'" ><i class="icon-remove"></i>&nbsp;删除</button>
											<a href="#myModal"  role="button" onclick="setvalue('${active.id}','${active.name}','${active.clubname }','${active.begintime}','${active.endtime }')" class="btn  btn-mini btn-info" data-toggle="modal"><i class="icon-edit"></i>编辑</a>
										</td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
					 		<div class="dataTables_paginate paging_bootstrap pagination">
							  <c:if test="${grid.pages > 1}">
							  <button class="btn btn-success btn-mini" onclick="searchActive(1)">首页</button>
							  <select name="classid" id="editclassid" class="btn btn-success input-medium" onchange="searchActive(this.value)">
							  	<c:forEach  varStatus="num" begin="1" end="${grid.pages }">
									<option value="${num.index }" <c:if test="${num.index ==grid.currentPage }"> selected="selected"</c:if>>第${num.index }页</option>
								</c:forEach>
							  </select>
							  <button class="btn btn-success btn-mini" onclick="searchActive(${grid.pages })" >末页</button>
							 </c:if>
					 		</div>
					 		<!-- Modal -->
							<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  	<form action="<%=request.getContextPath() %>/manager/updateactive" id="updateactive" method="post"  class="form-inline">
								  <div class="modal-header">
								    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								    <h3 id="myModalLabel">编辑活动</h3>
								  </div>
								  <div class="modal-body">
								  		<input type="hidden"  name="id"  id="editid" >
										<label class="control-label"   >活动名称:&nbsp;&nbsp;</label>
										<input type="text" name="name" class="input-medium"  id="editname"  >
										<br><br><label class="control-label"    >　俱乐部:&nbsp;&nbsp;</label>
										<input type="text" id="editclubname"  class="input-medium" readonly="readonly">
										<br><br><label class="control-label"    >开始时间:&nbsp;&nbsp;</label>
										<div class="input-append date form_datetime_start" >
										    <input size="16" type="text" value="" readonly name="startTime" id="editbegintime" class="input-medium">
										    <span class="add-on"><i class="icon-remove"></i></span>
										    <span class="add-on"><i class="icon-calendar"></i></span>
										</div>
										 <br><br><label class="control-label"    >结束时间:&nbsp;&nbsp;</label>
										 <div class="input-append date form_datetime_end" >
										    <input size="16" type="text" value="" readonly name="endTime" id="editendtime" class="input-medium">
										    <span class="add-on"><i class="icon-remove"></i></span>
										    <span class="add-on"><i class="icon-calendar"></i></span>
										</div>
								  </div>
								  <div class="modal-footer">
								    <button  type="button" id="modify" class="btn btn-small btn-primary">更新</button>
								  </div>
							  	</form>
							</div>
							<!--PAGE CONTENT ENDS-->
					</div><!--/.span-->
				</div><!--/.row-fluid-->
			</div><!--/.page-content-->
		</div><!--/.main-content-->
	</div><!--/.main-container-->
	<%@include file="/WEB-INF/views/common/js.jsp" %>
	<script type="text/javascript">
		$(function() {
			$('#modify').on('click', function() {
				if($.trim($("#editname").val())==''){
					alert('请输入活动名称！');
					return;
				}else if($.trim($("#editbegintime").val())==''){
					alert('请输入开始时间！');
					return;
				}else if($.trim($("#editendtime").val())==''){
					alert('请输入结束时间！');
					return;
				}else if($.trim($("#editbegintime").val())>$.trim($("#editendtime").val())){
					alert('开始时间不能大于结束时间！');
					return;
				}else{
					$("#updateactive").submit();
				}
			});
			$(".form_datetime_start").datetimepicker({
				language:'zh-CN',
		        format: "yyyy-mm-dd hh:ii:ss",
		        autoclose: true,
		        todayBtn: true
		    });
			$(".form_datetime_end").datetimepicker({
				language:'zh-CN',
		        format: "yyyy-mm-dd hh:ii:ss",
		        autoclose: true,
		        todayBtn: true
		    });
		});
		function setvalue(id,name,clubname,begintime,endtime){
			$("#editid").val(id);
			$("#editname").val(name);
			$("#editclubname").val(clubname);
			$("#editbegintime").val(begintime);
			$('.form_datetime_start').datetimepicker('update', new Date(begintime.replace(/-/g,"/")));
			$("#editendtime").val(endtime);
			$(".form_datetime_end").datetimepicker('update', new Date(endtime.replace(/-/g,"/")));
		}
		function searchActive(pagenum){
			$("#pagenum").val(pagenum);
			$("#searchActiveForm").submit();
		}
		
	</script>
	</body>
</html>