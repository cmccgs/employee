<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>添加俱乐部</title>
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<%@include file="/WEB-INF/views/common/css.jsp" %>
	</head>
	<body>
		<%@ include file="/WEB-INF/views/common/navbar.jsp" %>
		<div class="main-container container-fluid">
			<a class="menu-toggler" id="menu-toggler" href="#">
				<span class="menu-text"></span>
			</a>
			<%@ include file="/WEB-INF/views/common/sidebar.jsp" %>
			<div class="main-content">
				 
				<div class="page-content">
					<div class="page-header position-relative">
						<h1>
							<small>
								<i class="icon-list-alt"></i>
								添加俱乐部
							</small>
						</h1>
					</div> 
					<div class="row-fluid">
						
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<form action="addclub" id="addclub" class="form-horizontal" method="post">
								<div class="control-group">
									<span class="help-inline"> <font color="red"><c:if test="${param.notice!=null}">${param.notice}</c:if></font></span>
								</div>
								<div class="control-group">
									<label class="control-label"  for="name">俱乐部名称</label>
									<div class="controls">
										<input type="text" id="name" value="${param.name}" name="name" placeholder="俱乐部名称">*
									</div>
								</div>
								<div class="control-group">
									<label class="control-label"  for="des">俱乐部描述</label>
									<div class="controls">
										<input type="text" value="${param.des}" name="des" id="des" placeholder="俱乐部描述">*
									</div>
								</div>
									<div class="form-actions">
									<button class="btn btn-info" id="add" type="button">
										<i class="icon-ok bigger-110"></i>
										添加俱乐部
									</button>
									&nbsp; &nbsp; &nbsp;
									<button class="btn btn-info" type="button" onclick="location.href='<%=request.getContextPath() %>/manager/club'" >
										<i class="icon-arrow-left"></i>
										返回
									</button>
								</div>
							</form>
							<!--PAGE CONTENT ENDS-->
					</div><!--/.span-->
				</div><!--/.row-fluid-->
			</div><!--/.page-content-->
		</div><!--/.main-content-->
	</div><!--/.main-container-->
	<%@include file="/WEB-INF/views/common/js.jsp" %>
	<script type="text/javascript">
		$(function() {
			$('#add').on('click', function() {
				if($.trim($("#name").val())==''){
					alert('请输入俱乐部名称！');
					return;
				}else{
					$("#addclub").submit();
				}
			});
			
		});
		</script>
	</body>
</html>