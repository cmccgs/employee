<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>添加活动</title>
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<%@include file="/WEB-INF/views/common/css.jsp" %>
	</head>
	<body>
		<%@ include file="/WEB-INF/views/common/navbar.jsp" %>
		<div class="main-container container-fluid">
			<a class="menu-toggler" id="menu-toggler" href="#">
				<span class="menu-text"></span>
			</a>
			<%@ include file="/WEB-INF/views/common/sidebar.jsp" %>
			<div class="main-content">
				 
				<div class="page-content">
					<div class="page-header position-relative">
						<h1>
							<small>
								<i class="icon-list-alt"></i>
								添加签到活动
							</small>
						</h1>
					</div> 
					<div class="row-fluid">
						
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<form action="addPrizeActive" id="addPrizeActive" class="form-horizontal" method="post">
								<div class="control-group">
									<span class="help-inline"> <font color="red"><c:if test="${param.notice!=null}">${param.notice}</c:if></font></span>
								</div>
								<div class="control-group">
									<label class="control-label"  for="name">活动名称</label>
									<div class="controls">
										<input type="text" id="name" value="" name="name" placeholder="活动名称">*
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" >所需积分</label>
									<div class="controls">
										<input id="editscore" type="text" name="score" value="" placeholder="活动抽奖所需积分"/>*
									</div>
								</div>
								<div class="control-group">
									<label class="control-label"  for="des">开始时间</label>
									<div class="controls">
										<div class="input-append date form_datetime" >
										    <input size="16" type="text" value="" readonly name="startTime" id="starttime" class="input-medium">
										    <span class="add-on"><i class="icon-remove"></i></span>
										    <span class="add-on"><i class="icon-calendar"></i></span>
										</div>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label"  for="des">结束时间</label>
									<div class="controls">
										<div class="input-append date form_datetime" >
										    <input size="16" type="text" value="" readonly name="endTime" id="endtime" class="input-medium">
										    <span class="add-on"><i class="icon-remove"></i></span>
										    <span class="add-on"><i class="icon-calendar"></i></span>
										</div>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" >备注信息</label>
									<div class="controls">
										<input type="text" name="des" value="" placeholder="备注信息"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label"  for="editqd">是否启动</label>
									<input type="checkbox" id="editqd"/>
									<input type="hidden" name="qd" id="qd"/>
								</div>
								<div class="form-actions">
									<button class="btn btn-info" id="add" type="button">
										<i class="icon-ok bigger-110"></i>
										添加活动
									</button>
									&nbsp; &nbsp; &nbsp;
									<button class="btn btn-info" type="button" onclick="location.href='<%=request.getContextPath() %>/manager/prizeActive'" >
										<i class="icon-arrow-left"></i>
										返回
									</button>
								</div>
							</form>
							<!--PAGE CONTENT ENDS-->
					</div><!--/.span-->
				</div><!--/.row-fluid-->
			</div><!--/.page-content-->
		</div><!--/.main-content-->
	</div><!--/.main-container-->
	<%@include file="/WEB-INF/views/common/js.jsp" %>
	<script type="text/javascript">
		$(function() {
			$('#add').on('click', function() {
				var score = $("#editscore").val();
				var checked = $('#editqd').is(':checked');
				if(checked==true){
					$("#qd").val(1);
				}else{
					$("#qd").val(0);
				}
				if($.trim($("#name").val())==''){
					alert('请输入活动名称！');
					return;
				}else if($.trim(score)==''){
					alert('请输入活动所需积分！');
					return;
				}else if(isNaN(parseInt(score))){
					alert('活动所需积分必须为整数！');
					return;
				}else if($.trim($("#starttime").val())==''){
					alert('请输入开始时间！');
					return;
				}else if($.trim($("#endtime").val())==''){
					alert('请输入结束时间！');
					return;
				}else if($.trim($("#starttime").val())>$.trim($("#endtime").val())){
					alert('开始时间不能大于结束时间！');
					return;
				}else{
					$("#addPrizeActive").submit();
				}
			});
			$(".form_datetime").datetimepicker({
				language:'zh-CN',
		        format: "yyyy-mm-dd hh:ii:ss",
		        autoclose: true,
		        todayBtn: true
		    });
		});
		
		</script>
	</body>
</html>